package com.mvc.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class LikedBook {
	// Attributes
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@OneToOne
	private BookBean book;
	@OneToOne
	private UserBean user;

	public LikedBook() {
		super();
	}

	public LikedBook(BookBean book, UserBean user) {
		super();
		this.book = book;
		this.user = user;
	}

	public LikedBook(int id, BookBean book, UserBean user) {
		super();
		this.id = id;
		this.book = book;
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BookBean getBook() {
		return book;
	}

	public void setBook(BookBean book) {
		this.book = book;
	}

	public UserBean getUser() {
		return user;
	}

	public void setUser(UserBean user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "LikedBook [id=" + id + ", book=" + book + ", user=" + user + "]";
	}

}
