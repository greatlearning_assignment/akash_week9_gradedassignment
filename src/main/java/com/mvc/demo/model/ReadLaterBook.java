package com.mvc.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class ReadLaterBook {
	// Attributes
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@OneToOne
	private BookBean book;
	@OneToOne
	private UserBean user;

	public ReadLaterBook() {
		super();
	}

	public ReadLaterBook(BookBean book, UserBean user) {
		super();
		this.book = book;
		this.user = user;
	}

	public ReadLaterBook(int id, BookBean book, UserBean user) {
		super();
		this.id = id;
		this.book = book;
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BookBean getBook() {
		return book;
	}

	public void setBook(BookBean book) {
		this.book = book;
	}

	public UserBean getUser() {
		return user;
	}

	public void setUser(UserBean user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "ReadBook [id=" + id + ", book=" + book + ", user=" + user + "]";
	}

}
