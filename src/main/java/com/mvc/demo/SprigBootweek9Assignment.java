package com.mvc.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.mvc.demo.SprigBootweek9Assignment;
import com.mvc.demo.model.BookBean;
import com.mvc.demo.model.UserBean;
import com.mvc.demo.repositry.BookRepositry;
import com.mvc.demo.repositry.UserRepositry;

@SpringBootApplication
public class SprigBootweek9Assignment {
		@Autowired
		private BookRepositry bookRepositry;

		@Autowired
		private UserRepositry userRepositry;

		public static void main(String[] args) {
			SpringApplication.run(SprigBootweek9Assignment.class, args);
		}

		@Bean
		public void insertData() {
			BookBean book1 = new BookBean("Harry Potter", "J.K rollin", 200);
			BookBean book2 = new BookBean("CHand", "Akash", 400);
			BookBean book3 = new BookBean("Raat", "Yogesh", 500);
			BookBean book4 = new BookBean("Din", "Sonal", 488);

			bookRepositry.save(book1);
			bookRepositry.save(book2);
			bookRepositry.save(book3);
			bookRepositry.save(book4);

			UserBean user1 = new UserBean("Akash", "Akash@gmail.com", "Akash123", 1);
			UserBean user2 = new UserBean("Sonal", "sonal@gmail.com", "Sonal",2);


			userRepositry.save(user1);
			userRepositry.save(user2);

		}
	}
