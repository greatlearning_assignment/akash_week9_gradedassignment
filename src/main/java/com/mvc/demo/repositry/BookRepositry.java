package com.mvc.demo.repositry;

import org.springframework.data.repository.CrudRepository;

import com.mvc.demo.model.BookBean;

public interface BookRepositry extends CrudRepository<BookBean, Integer>{
    
}
