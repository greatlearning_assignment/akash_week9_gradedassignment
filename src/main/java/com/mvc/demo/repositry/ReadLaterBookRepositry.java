package com.mvc.demo.repositry;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mvc.demo.model.BookBean;
import com.mvc.demo.model.ReadLaterBook;

@Repository
public interface ReadLaterBookRepositry extends CrudRepository<ReadLaterBook, Integer> {
	public List<BookBean> findByUserId(int id);

	public boolean deleteAllByBookId(int id);

	public boolean existsByBookId(int id);
}
