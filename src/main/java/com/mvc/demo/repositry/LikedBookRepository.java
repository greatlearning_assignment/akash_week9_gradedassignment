package com.mvc.demo.repositry;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mvc.demo.model.BookBean;
import com.mvc.demo.model.LikedBook;

@Repository
public interface LikedBookRepository extends CrudRepository<LikedBook, Integer> {
	public List<BookBean> findByUserId(int id);

	public boolean deleteByBookId(int id);

	public boolean existsByBookId(int id);
}
