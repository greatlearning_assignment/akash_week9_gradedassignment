package com.mvc.demo.repositry;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mvc.demo.model.UserBean;

public interface UserRepositry extends CrudRepository<UserBean, Integer>{
     public UserBean findByEmail(String email);
     
     public List<UserBean> findByAdmin(int admin);
     public boolean existsByEmail(String email);
}
