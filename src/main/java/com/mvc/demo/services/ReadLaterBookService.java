package com.mvc.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvc.demo.model.ReadLaterBook;
import com.mvc.demo.repositry.ReadLaterBookRepositry;

@Service
public class ReadLaterBookService {
	 @Autowired
     private ReadLaterBookRepositry readBookRepositry;
     
     public List<ReadLaterBook> getAllReadLaterBook() {
    	 return (List<ReadLaterBook>) this.readBookRepositry.findAll();
     }
     
     public boolean insetReadLaterBook(ReadLaterBook readBook) {
    	 if(this.readBookRepositry.existsByBookId(readBook.getBook().getId())) {
    		 return false;
    	 }
    	 this.readBookRepositry.save(readBook);
    	 return true;
     }
}
