package com.mvc.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvc.demo.model.LikedBook;
import com.mvc.demo.repositry.LikedBookRepository;

@Service
public class LikedBookService {
	@Autowired
	private LikedBookRepository likedBookRepository;

	public List<LikedBook> getAllLikedBook() {
		return (List<LikedBook>) this.likedBookRepository.findAll();
	}

	public boolean insetLikedBook(LikedBook likedBook) {
		if (this.likedBookRepository.existsByBookId(likedBook.getBook().getId())) {
			return false;
		}
		this.likedBookRepository.save(likedBook);
		return true;
	}
}
