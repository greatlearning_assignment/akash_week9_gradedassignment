package com.mvc.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvc.demo.model.*;
import com.mvc.demo.repositry.BookRepositry;
import com.mvc.demo.repositry.LikedBookRepository;
import com.mvc.demo.repositry.ReadLaterBookRepositry;

@Service
public class BookBeanService {
	@Autowired
	private BookRepositry bookRepositry;

	@Autowired
	private LikedBookRepository likedBookRepositry;

	@Autowired
	private ReadLaterBookRepositry readBookRepositry;

	public List<BookBean> getAllBook() throws Exception {
		return (List<BookBean>) this.bookRepositry.findAll();
	}

	public BookBean getBookById(int bookId) {
		return this.bookRepositry.findById(bookId).get();
	}

	public Boolean addBook(BookBean book) {
		this.bookRepositry.save(book);
		return true;
	}

	public boolean updateBook(BookBean BookBean) {
		if (this.bookRepositry.existsById(BookBean.getId())) {
			this.bookRepositry.save(BookBean);
			return true;
		}
		return false;
	}

	public boolean deleteBook(int bookId) {
		System.out.println(bookId);
		if (this.bookRepositry.existsById(bookId)) {
			System.out.println("book list page " + bookId);
			if (this.likedBookRepositry.existsByBookId(bookId)) {
				System.out.println("liked book list page " + bookId);

				this.likedBookRepositry.deleteByBookId(bookId);
			}

			if (this.readBookRepositry.existsByBookId(bookId)) {
				System.out.println("read book list page " + bookId);

				this.readBookRepositry.deleteAllByBookId(bookId);
			}
			this.bookRepositry.deleteById(bookId);
			return true;
		}
		return false;
	}

}
