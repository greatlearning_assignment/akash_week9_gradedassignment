package com.mvc.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvc.demo.model.UserBean;
import com.mvc.demo.repositry.UserRepositry;

@Service
public class UserBeanService {
	@Autowired
	private UserRepositry userRepositry;

	public UserBean getUserById(int userId) {
		return this.userRepositry.findById(userId).get();
	}

	public boolean insertUser(UserBean user) {
		if (this.userRepositry.existsByEmail(user.getEmail())) {
			return false;
		}
		this.userRepositry.save(user);
		return true;
	}

	public UserBean getUserBean(String email) throws Exception {
		return this.userRepositry.findByEmail(email);
	}

	public boolean userValidation(UserBean user) throws Exception {
		UserBean existUser = null;
		try {
			existUser = getUserBean(user.getEmail());
			if (user.getPassword().equals(existUser.getPassword())) {
				return true;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}
		return false;
	}

	public List<UserBean> getAllUser() {
		return this.userRepositry.findByAdmin(0);
	}

	public boolean deleteUser(int userId) {
		if (this.userRepositry.existsById(userId)) {
			this.userRepositry.deleteById(userId);
			return true;
		} else {
			return false;
		}
	}
	
	public boolean updateUser(UserBean user) {
		if(this.userRepositry.existsById(user.getId())) {
			this.userRepositry.save(user);
			return true;
		}else {
			return false;
		}
		
	}
}
