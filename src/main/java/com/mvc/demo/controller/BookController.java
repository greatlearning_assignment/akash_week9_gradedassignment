package com.mvc.demo.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mvc.demo.model.*;
import com.mvc.demo.services.BookBeanService;

@Controller
public class BookController {
	@Autowired
	private BookBeanService bookBeanService;


	@GetMapping("/bookList")
	public String getBookList(Map<String, List<BookBean>> map){
		try {
			List<BookBean> books = bookBeanService.getAllBook();
			map.put("books", books);
			return "bookList";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("can not find book list");
			e.printStackTrace();
		}
		return "bookList";
	}
	
	@GetMapping("/dashboard")
	public String getDashboard(@RequestParam(required = false) String error,Map<String, List<BookBean>> map, Map<String, String> msg, HttpSession httpSession){
		try {
			List<BookBean> books = bookBeanService.getAllBook();
			map.put("books", books);
			if(error != null) {
				msg.put("error", error);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("can not find book list");
			e.printStackTrace();
		}
		return "dashboard";
	}
	
	
	@GetMapping("/adminDashboard")
	public String getAdminDashboard(@RequestParam(required = false) String error,Map<String, List<BookBean>> map,Map<String, String> msg, HttpSession httpSession){
		try {
			List<BookBean> books = bookBeanService.getAllBook();
			map.put("books", books);
			if(error != null) {
				System.out.println(error);
				msg.put("error", error);
			}
		} catch (Exception e) {
			System.out.println("can not find book list");
			e.printStackTrace();
		}
		return "adminDashboard";
	}
	
	@GetMapping("/addBook")
	public String getAddBookIndex() {
		return "addBook";
	}
	
	
	@PostMapping("/addBook")
	public String getAddBook(BookBean book, Map<String, List<BookBean>> map) throws Exception {
	    if(bookBeanService.addBook(book)) {
			List<BookBean> books = bookBeanService.getAllBook();
			map.put("books", books);
			return "redirect:adminDashboard?error=book has been successfully added";
		}else {
			return "addBook";
		}
		
	}
	
	@GetMapping("/editBook")
	public String getEdit(@RequestParam int bookId, Map<String, BookBean> map, HttpSession httpSession) {
		httpSession.setAttribute("ediBbookId", bookId);
		BookBean book = bookBeanService.getBookById(bookId);
		map.put("book", book);
		return "editBook";
	}
	
	@PostMapping("/editBook")
	public String editBook(BookBean book,Map<String, List<BookBean>> map,HttpSession httpSession) throws Exception {
		int bookId = (int)httpSession.getAttribute("ediBbookId");
		book.setId(bookId);
		if (bookBeanService.updateBook(book)) {
			List<BookBean> books = bookBeanService.getAllBook();
			map.put("books", books);
			return "redirect:adminDashboard?error=Book is Updated";
		} else {
			return "redirect:adminDashboard?error=Book can not Update";
		}
	}
	
	@GetMapping("/deleteBook")
	public String deleteBook(@RequestParam int bookId,Map<String, List<BookBean>> map) {
		if(this.bookBeanService.deleteBook(bookId)) {
			return "redirect:adminDashboard?error=Book is deleted";
		}else {
			return "redirect:adminDashboard?error=book is not deleted";
		}
		
	}
}
