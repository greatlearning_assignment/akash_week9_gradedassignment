package com.mvc.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mvc.demo.model.BookBean;
import com.mvc.demo.model.LikedBook;
import com.mvc.demo.services.BookBeanService;
import com.mvc.demo.services.LikedBookService;
import com.mvc.demo.services.UserBeanService;

@Controller
public class LikedBookController {
	@Autowired
	private LikedBookService likedBookService;

	@Autowired
	private BookBeanService bookBeanService;

	@Autowired
	private UserBeanService userBeanService;

	@GetMapping("/likedBook")
	public String getReadLaterBook(Map<String, List<BookBean>> map, HttpSession httpSession) {
		List<BookBean> likedBookList = new ArrayList<BookBean>();
		List<LikedBook> likedBooks = likedBookService.getAllLikedBook();
        
		System.out.println(likedBooks);
		if (!likedBooks.isEmpty()) {
			for (LikedBook book : likedBooks) {
				System.out.println(book.getBook());
				likedBookList.add(book.getBook());
			}
			map.put("likedBookList", likedBookList);
			System.out.println(map);
			return "likedBookList";
		} else {
			return "redirect:dashboard?error=Liked book list is empty";
		}

	}

	@GetMapping("/likedBookInsert")
	public String insertReadLaterBook(@RequestParam int bookId, HttpSession httpSession) {
		int userId = (int) httpSession.getAttribute("userId");
		LikedBook likedBook = new LikedBook(bookBeanService.getBookById(bookId), userBeanService.getUserById(userId));
		if (likedBookService.insetLikedBook(likedBook)) {
			return "redirect:dashboard?error=Book is Successfully added in liked list";
		} else {
			return "redirect:dashboard?error=This book is alrady added ";
		}
	}
}
