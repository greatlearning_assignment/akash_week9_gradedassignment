package com.mvc.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mvc.demo.model.BookBean;
import com.mvc.demo.model.ReadLaterBook;
import com.mvc.demo.services.BookBeanService;
import com.mvc.demo.services.ReadLaterBookService;
import com.mvc.demo.services.UserBeanService;

@Controller
public class ReadLaterBookController {
	@Autowired
	private ReadLaterBookService readBookService;

	@Autowired
	private BookBeanService bookBeanService;

	@Autowired
	private UserBeanService userBeanService;

	@GetMapping("/readLater")
	public String getReadLaterBook(Map<String, List<BookBean>> map, HttpSession httpSession) {
		List<BookBean> readLaterBooks = new ArrayList<BookBean>();
		List<ReadLaterBook> readBooks = readBookService.getAllReadLaterBook();
		if (!readBooks.isEmpty()) {
			for (ReadLaterBook book : readBooks) {
				System.out.println(book.getBook());
				readLaterBooks.add(book.getBook());
			}
			map.put("readLaterBooks", readLaterBooks);
			return "readLaterList";
		} else {
			return "redirect:dashboard?error=Read Book list is empty";
		}

	}

	@GetMapping("/read")
	public String insertReadLaterBook(@RequestParam int bookId, HttpSession httpSession) {
		int userId = (int) httpSession.getAttribute("userId");
		ReadLaterBook readBook = new ReadLaterBook(bookBeanService.getBookById(bookId), userBeanService.getUserById(userId));
		if (readBookService.insetReadLaterBook(readBook)) {
			return "redirect:dashboard?error=Book is successfully added in read Later list";
		} else {
			return "redirect:dashboard?error=Book is alrady added in list";
		}
	}
}
