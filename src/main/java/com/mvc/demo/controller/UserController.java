package com.mvc.demo.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mvc.demo.model.UserBean;
import com.mvc.demo.services.UserBeanService;

@Controller
public class UserController {

	@Autowired
	private UserBeanService userBeanService;

	@GetMapping("/index")
	public String getIndex() {
		return "index";
	}

	@GetMapping("/login")
	public String getLogin(@RequestParam(required = false) String error, Map<String, String> map) {
		if (error != null) {
			map.put("error", error);
		}
		return "login";
	}

	@PostMapping("/login")
	public String postLogin(@RequestParam(required = false) String register, UserBean userBean, HttpSession session) {
		if (register == null) {
			try {
				System.out.println(userBean);
				if (userBeanService.userValidation(userBean)) {
					session.setAttribute("name", userBeanService.getUserBean(userBean.getEmail()).getName());
					session.setAttribute("email", userBeanService.getUserBean(userBean.getEmail()).getEmail());
					session.setAttribute("userId", userBeanService.getUserBean(userBean.getEmail()).getId());
					if (userBeanService.getUserBean(userBean.getEmail()).getAdmin() == 1) {
						return "redirect:adminDashboard";
					} else {
						return "redirect:dashboard";
					}

				} else {
					return "redirect:login?error=Invalid creditionals";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block

				return "redirect:login?error=User not register!";

			}

		} else if (register.equals("Register")) {
			return "redirect:register";
		} else {
			return "redirect:adminRegister";
		}
	}

	@GetMapping("/register")
	public String getRegister(@RequestParam(required = false) String error, Map<String, String> map) {
		if (error != null) {
			map.put("error", error);
		}
		return "register";
	}

	@PostMapping("/register")
	public String userRegister(UserBean userBean) {
		if (userBeanService.insertUser(userBean)) {
			return "redirect:login?error=You are successfully register";
		} else {
			return "redirect:register?error=You are alrady register";
		}

	}

	@GetMapping("/adminRegister")
	public String getAdminRegisterPage(@RequestParam(required = false) String error, Map<String, String> map) {
		if (error != null) {
			map.put("error", error);
		}
		return "adminRegister";
	}

	@PostMapping("/adminRegister")
	public String getAdminRegister(UserBean userBean) {
		System.out.println(userBean);
		if (userBeanService.insertUser(userBean)) {
			return "redirect:login?error=You have successfuly rigister!";
		} else {
			return "redirect:adminRegister?error=You have alrady rigister!";
		}
	}

	@GetMapping("/logout")
	public String getLogout(HttpSession httpSession) {
		httpSession.removeAttribute("email");
		httpSession.removeAttribute("userId");
		httpSession.removeAttribute("name");
		return "login";
	}

	@GetMapping("/allUsers")
	public String getAllUsers(@RequestParam(required = false) String error, Map<String, List<UserBean>> map,
			Map<String, String> msg) {
		List<UserBean> users = this.userBeanService.getAllUser();
		if (!users.isEmpty()) {
			map.put("users", users);
			msg.put("error", error);
			return "userList";
		} else {
			return "redirect:adminDashboard?error=User list is empty";
		}
	}

	@GetMapping("/deleteUser")
	public String deleteUser(@RequestParam int userId, Map<String, List<UserBean>> map) {
		if (this.userBeanService.deleteUser(userId)) {
			return "redirect:allUsers?error=You successfuly deleted the user";
		}
		return "redirect:allUsers?error=You are not able to delete this user";
	}

	@GetMapping("/editUser")
	public String getUser(int userId, Map<String, UserBean> map, HttpSession httpSession) {
		UserBean user = this.userBeanService.getUserById(userId);
		httpSession.setAttribute("editUserId", userId);
		if (user != null) {
			map.put("user", user);
			return "editUser";
		} else {
			return "redirect:allUsers?error=You can not edit this user";
		}
	}

	@PostMapping("/editUser")
	public String editUser(UserBean user, Map<String, List<UserBean>> map, HttpSession httpSession) {
		user.setId((int)httpSession.getAttribute("editUserId"));
		if(this.userBeanService.updateUser(user)) {
			List<UserBean> users = this.userBeanService.getAllUser();
			map.put("users", users);
			return "redirect:allUsers?error=You are successfuly updated user";
			
		}
		return "redirect:allUsers?error=You con not update this user";
	}

}
