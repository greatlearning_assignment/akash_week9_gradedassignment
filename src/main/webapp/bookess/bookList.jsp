<%@page import="com.mvc.demo.model.BookBean"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book List</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<style>
body {
	background-image: url("Background.jpg");
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
}

</style>
</head>
<body>
	<!-- header -->
	<nav class="navbar navbar-dark justify-content-between">
		<h1 class="btn btn-outline-info my-2 my-sm-0">Book List</h1>
		<form class="form-inline">
			<button class="btn btn-outline-info my-2 my-sm-0">
				<a style="text-decoration: none;" class="text-muted" href="login" id="">Login</a>
			</button>
		</form>
	</nav>
	<div class='container'>
		<div class="row">
			<table class="table table-dark mt-3">
				<thead>
					<tr>
						<th scope="col">Book Id</th>
						<th scope="col">Book Name</th>
						<th scope="col">Book Author</th>
						<th scope="col">Book Price</th>
					</tr>
				</thead>
				<tbody class="text-primary">
					<%
						List<BookBean> books = (List<BookBean>) request.getAttribute("books");

						for (BookBean book : books) {
					%>
					<tr>
						<td><p><%=book.getId()%></p></td>
						<td><p><%=book.getName()%></p></td>
						<td><p><%=book.getAuthor()%></p></td>
						<td><p><%=book.getPrice()%></p></td>
					</tr>
					<%
						}
					%>
					
				</tbody>
			</table>
		</div>
	</div>

</html>