<%@page import="com.mvc.demo.model.UserBean"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book List</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<style>
body {
	background-image: url("Background.jpg");
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
}

</style>
</head>
<body>
	<nav class="navbar navbar-dark justify-content-between">
		<h1 class="btn btn-outline-info my-2 my-sm-0">User List</h1>
		<form class="form-inline">
			
		</form>
	</nav>
	<div class='container'>
		<div class="row">
			<table class="table table-dark mt-3">
				<thead>
					<tr>
						<th scope="col">User Id</th>
						<th scope="col">User Name</th>
						<th scope="col">User Email</th>
						<th scope="col">Edit</th>
						<th scope="col">Delete</th>
						</tr>
				</thead>
				<tbody class="text-primary">
					<%
						List<UserBean> users = (List<UserBean>) request.getAttribute("users");

						for (UserBean user : users) {
					%>
					<tr>
						<td><p><%=user.getId()%></p></td>
						<td><p><%=user.getName()%></p></td>
						<td><p><%=user.getEmail()%></p></td>
						<td><a href='editUser?userId=<%=user.getId()%>'><i
								class="fa fa-edit text-warning"
								style="font-size: 24px; text-decoration: none;"></i></a></td>
						<td><a href='deleteUser?userId=<%=user.getId()%>'><i
								class="fa fa-trash text-danger" style="font-size: 24px;"></i></a></td>
					
					
						</tr>
					<%
						}
					%>
					
				</tbody>
			</table>
		</div>
	</div>

</body>
</html>